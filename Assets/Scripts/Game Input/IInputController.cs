﻿using UnityEngine;

namespace GameInput
{
    public interface IInputController
    {
        Vector2 CameraAxis { get; }
        Vector2 MoveAxis { get; }
        bool IsFireButonPressed { get; }
        bool InteractionButtonWasPressed { get; }
        bool HealButtonWasPressed { get; }
    }
}