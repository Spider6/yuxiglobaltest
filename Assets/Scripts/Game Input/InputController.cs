using UnityEngine;

namespace GameInput.Detail
{
    public class InputController : IInputController
    {
        public Vector2 CameraAxis
        {
            get { return new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); }
        }

        public Vector2 MoveAxis
        {
            get { return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); }
        }

        public bool IsFireButonPressed
        {
            get { return Input.GetButton("Fire1"); }
        }

        public bool InteractionButtonWasPressed
        {
            get { return Input.GetButtonDown("Interaction"); }
        }

        public bool HealButtonWasPressed
        {
            get { return Input.GetButtonDown("Heal"); }
        }
    }
}