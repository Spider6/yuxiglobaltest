using Zenject;

namespace GameInput.Detail.ZenjectInstaller
{
    public class GameInputInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<InputController>().AsSingle();
            Container.Bind<IInputController>().To<InputController>().AsSingle();
        }
    }
}