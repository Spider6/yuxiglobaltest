﻿using UnityEngine;

namespace Battle.Detail
{
    public class DamageGeneratorSettings : ScriptableObject
    {
        [SerializeField]
        private float damage;
        
        public float Damage
        {
            get { return damage; }
        }
    }
}
