﻿using UnityEngine;

namespace Battle.Detail
{
    public class DamageGenerator : MonoBehaviour
    {
        [SerializeField]
        private DamageGeneratorSettings settings;
        
        private void OnTriggerEnter(Collider other)
        {
            HealthController newVictim = other.gameObject.GetComponent<HealthController>();
            if (newVictim != null)
                newVictim.SetDamage(settings.Damage);
        
        
        }
    }
}
