﻿using UnityEngine;

namespace Battle.Detail
{
    public class DistanceWeaponSettings : ScriptableObject
    {
        [SerializeField]
        private float shootingDecay;
        [SerializeField]
        private Projectile projectilePrefab;

        public float ShootingDecay
        {
            get { return shootingDecay; }
        }

        public Projectile ProjectilePrefab
        {
            get { return projectilePrefab; }
        }
    }
}
