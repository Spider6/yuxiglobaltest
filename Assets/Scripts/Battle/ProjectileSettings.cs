﻿using UnityEngine;

namespace Battle.Detail
{
    public class ProjectileSettings : ScriptableObject
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float damage;
        [SerializeField]
        private float lifeTime;

        public float Speed
        {
            get { return speed; }
        }

        public float Damage
        {
            get { return damage; }
        }

        public float LifeTime
        {
            get { return lifeTime; }
        }
    }
}
