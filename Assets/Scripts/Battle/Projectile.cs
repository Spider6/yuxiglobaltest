﻿using Battle.Detail;
using UnityEngine;

namespace Battle
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField]
        private ProjectileSettings settings;
        [SerializeField]
        private Rigidbody myRigidbody;
        [SerializeField]
        private Transform myTransform;

        private int layerIgnore;
        private float timeToDestroy;
        private bool isActive;
        
        public void OnShoot(int layerIgnore)
        {
            this.layerIgnore = layerIgnore;
            timeToDestroy = Time.realtimeSinceStartup + settings.LifeTime;
            isActive = true;
        }

        private void Update()
        {
            if (Time.realtimeSinceStartup >= timeToDestroy)
                Destroy(gameObject);
            if(isActive)
                myRigidbody.velocity = myTransform.forward * settings.Speed;   
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.layer != layerIgnore && isActive)
            {
                HealthController health = collision.gameObject.GetComponent<HealthController>();
                if (health != null)
                {
                    health.SetDamage(settings.Damage);
                    Destroy(gameObject);
                }

                isActive = false;
            }
        }
    }
}
