﻿using UnityEngine;
using System;
using Battle.Detail;

namespace Battle
{
    public class HealthController : MonoBehaviour
    {
        public event Action DiedDelegate;
        public event Action HitDelegate;
        public event Action HealedDelegate;

        [SerializeField]
        private HealthControllerSettings settings;

        public float CurrentHealth
        {
            get;
            private set;
        }
        
        public float MaxHealth
        {
            get { return settings.MaxHealth; }
        }

        public void Initialize()
        {
            CurrentHealth = MaxHealth;
        }

        public void Heal(float healAmount)
        {
            CurrentHealth = Mathf.Clamp(healAmount + CurrentHealth, 0, MaxHealth);
            if (HealedDelegate != null)
                HealedDelegate();
        }

        public void SetDamage(float damage)
        {
            if (CurrentHealth > 0)
            {
                CurrentHealth -= damage;
                if (CurrentHealth <= 0)
                {
                    if (DiedDelegate != null)
                        DiedDelegate();
                }
                else if (HitDelegate != null)
                    HitDelegate();
            }
        }
    }
}
