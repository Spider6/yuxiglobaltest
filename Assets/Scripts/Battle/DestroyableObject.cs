﻿using UnityEngine;

namespace Battle.Detail
{
    public class DestroyableObject : MonoBehaviour
    {
        [SerializeField]
        private HealthController healthController;

        private void Awake()
        {
            healthController.DiedDelegate += OnDied;
            healthController.Initialize();
        }

        private void OnDied()
        {
            Destroy(gameObject);
        }
    }
}
