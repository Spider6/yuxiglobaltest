﻿using UnityEngine;

namespace Battle.Detail
{
    public class HealthControllerSettings : ScriptableObject
    {
        [SerializeField]
        private float maxHealth;

        public float MaxHealth
        {
            get { return maxHealth; }
        }
    }
}
