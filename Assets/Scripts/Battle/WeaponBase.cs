﻿using UnityEngine;

namespace Battle
{
    public abstract class WeaponBase : MonoBehaviour
    {
        protected abstract bool CanAttack
        {
            get;
        }
        
        public void TryAttack(int layerIgnore)
        {
            if (CanAttack)
                Attack(layerIgnore);
        }

        protected abstract void Attack(int layerIgnore);
    }
}
