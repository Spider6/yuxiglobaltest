﻿using UnityEngine;

namespace Battle.Detail
{
    public class DistanceWeapon : WeaponBase
    {
        [SerializeField]
        private DistanceWeaponSettings settings;
        [SerializeField]
        private Transform shootPoint;

        private float timeToShoot;

        protected override bool CanAttack
        {
            get { return Time.realtimeSinceStartup >= timeToShoot; }
        }

        protected override void Attack(int layerIgnore)
        {
            timeToShoot = Time.realtimeSinceStartup + settings.ShootingDecay;
            Projectile projectile = Instantiate(settings.ProjectilePrefab);
            projectile.transform.position = shootPoint.position;
            projectile.transform.rotation = shootPoint.rotation;
            projectile.OnShoot(layerIgnore);
        }
    }
}
