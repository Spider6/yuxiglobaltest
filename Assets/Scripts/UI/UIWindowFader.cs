﻿using UnityEngine;
using Util;
using System;
using UIController;

namespace UI.Detail
{
    public class UIWindowFader : MonoBehaviour, IWindowFader
    {
        [SerializeField]
        private CanvasGroup content;
        [SerializeField]
        private TweenerSettings settings;

        private TweenerController tweenerController = new TweenerController();

        public void FadeIn(Action callback)
        {
            content.alpha = 0;
            tweenerController.Fade(content, 1, settings, callback);
        }

        public void FadeOut(Action callback)
        {
            tweenerController.Fade(content, 0, settings, callback);
        }
    }
}
