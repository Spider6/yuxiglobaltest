﻿using Interaction;
using UIController;
using UnityEngine;

namespace UI.Detail
{
    public class UIInventorySetter : UITextSetter, IInventorySetter
    {
        [SerializeField]
        private InteractiveObjectType type;

        public void SetUp(InteractiveObjectType type, int amount)
        {
            if (this.type == type)
            {
                SetUp(amount.ToString());
            }
        }
    }
}
