﻿using UIController;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Detail
{
    public class UITextSetter : MonoBehaviour, ITextSetter
    {
        [SerializeField]
        private Text textLabel;

        public void SetUp(string newText)
        {
            textLabel.text = newText;
        }
    }
}
