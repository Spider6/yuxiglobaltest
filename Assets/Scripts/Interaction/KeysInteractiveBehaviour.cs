﻿using UnityEngine;

namespace Interaction.Detail
{
    [System.Serializable]
    public class KeysInteractiveBehaviour : InteractiveObjectBehaviour
    {
        [SerializeField]
        private InteractiveObjectBehaviourSettings settings;

        public override InteractiveObjectType Type
        {
            get { return InteractiveObjectType.KEYS;  }
        }
        
        public void Initialize()
        {
            Amount = settings.Amount;
        }
    }
}
