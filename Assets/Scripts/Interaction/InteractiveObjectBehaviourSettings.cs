﻿using UnityEngine;

namespace Interaction.Detail
{
    public class InteractiveObjectBehaviourSettings : ScriptableObject
    {
        [SerializeField]
        private int amount;

        public int Amount
        {
            get { return amount; }
        }
    }
}
