﻿using UnityEngine;
using Util;

namespace Interaction.Detail
{
    public class DoorInteractiveObject : InteractiveObject
    {
        [SerializeField]
        private RotationTweenerSettings rotationSettings;
        [SerializeField]
        private Transform content;

        private bool canInteract = true;
        private TweenerController tweenController = new TweenerController();

        public override bool CanInteract
        {
            get { return canInteract && base.CanInteract; }
            protected set { base.CanInteract = value;  }
        }

        public override InteractiveObjectBehaviour Behaviour
        {
            get { return null; }
        }

        public override bool CanPickUp
        {
            get { return false; }
        }

        public override InteractiveObjectType TypeToUse
        {
            get { return InteractiveObjectType.KEYS; }
        }

        public override void StartInteraction()
        {
            canInteract = false;
            tweenController.Rotate(content, rotationSettings, null);
            Unfocus();
        }
    }
}
