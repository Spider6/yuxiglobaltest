﻿using UnityEngine;

namespace Interaction.Detail
{
    public class HealBottleObjectBehaviourSettings : InteractiveObjectBehaviourSettings
    {
        [SerializeField]
        private float healAmount;

        public float HealAmount
        {
            get { return healAmount; }
        }
    }
}
