﻿using Zenject;

namespace Interaction.Detail.Installer
{
    public class InteractionInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<Inventory>().AsSingle();
            Container.Bind<IInventory>().To<Inventory>().AsSingle();
        }
    }
}
