﻿using UnityEngine;

namespace Interaction.Detail
{
    public class InteractiveAgentSettings : ScriptableObject
    {
        [SerializeField]
        private float rayCastMaxDistance;

        public float RayCastMaxDistance
        {
            get { return rayCastMaxDistance; }
        }
    }
}
