﻿namespace Interaction
{
    public enum InteractiveObjectType
    {
        NONE = 0,
        KEYS = 1,
        HEALTH_BOTTLES = 2,
    }
}
