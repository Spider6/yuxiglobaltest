﻿using UnityEngine;

namespace Interaction.Detail
{
    public class KeysInteractiveObject : InteractiveObject
    {
        [SerializeField]
        private KeysInteractiveBehaviour behaviour;

        public override InteractiveObjectBehaviour Behaviour
        {
            get { return behaviour;  }
        }

        public override bool CanPickUp
        {
            get { return true; }
        }

        public override InteractiveObjectType TypeToUse
        {
            get { return InteractiveObjectType.NONE; }
        }
                
        public override void StartInteraction()
        {
            Destroy(gameObject);
        }

        private void Start()
        {
            behaviour.Initialize();
        }
    }
}
