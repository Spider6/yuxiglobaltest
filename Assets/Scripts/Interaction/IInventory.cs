﻿using System;

namespace Interaction
{
    public interface IInventory
    {
        event Action<InteractiveObjectType> ChangedInventoryDelegate;

        int GetAmountOfType(InteractiveObjectType type);
        bool HasObjectOfType(InteractiveObjectType type);
        void UseObjectOfType(InteractiveObjectType type);
    }
}