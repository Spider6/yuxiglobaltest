﻿using UnityEngine;

namespace Interaction.Detail
{
    [System.Serializable]
    public class LevelDoorItem
    {
        [SerializeField]
        private Transform content;
        [SerializeField]
        private Transform openPoint;

        public Transform Content
        {
            get { return content; }
        }

        public Transform OpenPoint
        {
            get { return openPoint; }
        }
    }
}
