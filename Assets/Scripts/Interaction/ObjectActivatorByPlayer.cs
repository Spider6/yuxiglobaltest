﻿using UnityEngine;
using Util;
using Zenject;

namespace Interaction.Detail
{
    public class ObjectActivatorByPlayer : MonoBehaviour
    {
        [SerializeField]
        private GameObject content;

        [Inject]
        private LayerController layerController;

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.layer == layerController.PlayerLayer)
                content.SetActive(true);
        }

        private void Awake()
        {
            content.SetActive(false);
        }
    }
}
