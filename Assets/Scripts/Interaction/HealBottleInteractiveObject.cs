﻿using Player;
using Zenject;
using UnityEngine;

namespace Interaction.Detail
{
    public class HealBottleInteractiveObject : InteractiveObject
    {
        [SerializeField]
        private HealBottleInteractiveBehaviour behaviour;

        [Inject]
        private IPlayerControllerForHeal playerController;

        public override InteractiveObjectBehaviour Behaviour
        {
            get { return behaviour; }
        }

        public override bool CanPickUp
        {
            get { return true; }
        }

        public override InteractiveObjectType TypeToUse
        {
            get { return InteractiveObjectType.NONE; }
        }

        public override void StartInteraction()
        {
            Destroy(gameObject);
        }

        [Inject]
        private void PostInject()
        {
            behaviour.Initialize(playerController);
        }
    }
}
