﻿namespace Interaction.Detail
{
    public abstract class InteractiveObjectBehaviour
    {
        public abstract InteractiveObjectType Type { get; }

        public virtual int Amount
        {
            get;
            set;
        }

        public virtual void Use()
        {
            Amount--;
        }
    }
}
