﻿using UnityEngine;

namespace Interaction.Detail
{
    public abstract class InteractiveObject : MonoBehaviour
    {
        [SerializeField]
        private GameObject focusContent;

        public abstract InteractiveObjectBehaviour Behaviour { get; }
        public abstract bool CanPickUp { get; }
        public abstract InteractiveObjectType TypeToUse { get; }

        public virtual bool CanInteract
        {
            get;
            protected set;
        }

        public abstract void StartInteraction();

        public void Focus()
        {
            CanInteract = true;
            if (CanInteract && focusContent != null)
                focusContent.SetActive(true);
        }

        public void Unfocus()
        {
            CanInteract = false;
            if (focusContent != null)
                focusContent.SetActive(false);
        }

        private void Awake()
        {
            Unfocus();
        }
    }
}
