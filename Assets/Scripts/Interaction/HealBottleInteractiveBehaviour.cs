﻿using Player;
using UnityEngine;

namespace Interaction.Detail
{
    [System.Serializable]
    public class HealBottleInteractiveBehaviour : InteractiveObjectBehaviour
    {
        [SerializeField]
        private HealBottleObjectBehaviourSettings settings;

        private IPlayerControllerForHeal playerController;

        public override InteractiveObjectType Type
        {
            get { return InteractiveObjectType.HEALTH_BOTTLES; }
        }
        
        public void Initialize(IPlayerControllerForHeal playerController)
        {
            this.playerController = playerController;
            Amount = settings.Amount;
        }

        public override void Use()
        {
            base.Use();
            playerController.Heal(settings.HealAmount);
        }
    }
}
