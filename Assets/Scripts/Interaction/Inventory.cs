﻿using System;
using System.Collections.Generic;

namespace Interaction.Detail
{
    public class Inventory : IInventory
    {
        public event Action<InteractiveObjectType> ChangedInventoryDelegate;

        private List<InteractiveObjectBehaviour> objects =  new List<InteractiveObjectBehaviour>();

        public int GetAmountOfType(InteractiveObjectType type)
        {
            int amount = 0;
            List<InteractiveObjectBehaviour> behaviours = objects.FindAll(o => o.Type == type);
            behaviours.ForEach(b => amount += b.Amount);
            return amount;
        }

        public bool HasObjectOfType(InteractiveObjectType type)
        {
            return type == InteractiveObjectType.NONE ? true : GetAmountOfType(type) > 0;
        }

        public void UseObjectOfType(InteractiveObjectType type)
        {
            if(type != InteractiveObjectType.NONE)
            {
                InteractiveObjectBehaviour behaviour = objects.Find(o => o.Type == type);
                behaviour.Use();
                if (behaviour.Amount <= 0)
                    objects.Remove(behaviour);
                OnChangedInventory(type);
            }
        }

        public void Add(InteractiveObjectBehaviour behaviour)
        {
            objects.Add(behaviour);
            OnChangedInventory(behaviour.Type);
        }

        private void OnChangedInventory(InteractiveObjectType type)
        {
            if (ChangedInventoryDelegate != null)
                ChangedInventoryDelegate(type);
        }
    }
}
