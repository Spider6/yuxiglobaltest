﻿using UnityEngine;
using Zenject;
using GameInput;
using Util;

namespace Interaction.Detail
{
    public class InteractiveAgent : MonoBehaviour
    {
        [SerializeField]
        private Transform raycastPoint;
        [SerializeField]
        private InteractiveAgentSettings settings;

        [Inject]
        private IInputController inputController;
        [Inject]
        private Inventory inventory;
        [Inject]
        private LayerController layerController;

        private InteractiveObject currentObject;

        private void Update()
        {
            CheckObject();
            CheckInteraction();
        }

        private void CheckInteraction()
        {
            if (currentObject != null && currentObject.CanInteract && inputController.InteractionButtonWasPressed)
            {
                if (inventory.HasObjectOfType(currentObject.TypeToUse))
                {
                    if (currentObject.CanPickUp)
                        inventory.Add(currentObject.Behaviour);

                    inventory.UseObjectOfType(currentObject.TypeToUse);
                    currentObject.StartInteraction();
                }
            }
        }

        private void CheckObject()
        {
            Ray ray = new Ray(raycastPoint.position, raycastPoint.forward);
            RaycastHit hit;
            Debug.DrawRay(raycastPoint.position, raycastPoint.forward * settings.RayCastMaxDistance, Color.red);
            if (Physics.Raycast(ray, out hit, settings.RayCastMaxDistance))
            {
                if (hit.collider.gameObject.layer == layerController.InteractiveLayer)
                    OnHitInteractiveObject(hit);
                else
                    UnfocusObject();
            }
            else
                UnfocusObject();
        }

        private void UnfocusObject()
        {
            if (currentObject != null)
            {
                currentObject.Unfocus();
                currentObject = null;
            }
        }

        private void OnHitInteractiveObject(RaycastHit hit)
        {
            if (currentObject == null || currentObject.gameObject != hit.collider.gameObject)
            {
                if (currentObject != null)
                    currentObject.Unfocus();
                currentObject = hit.collider.GetComponent<InteractiveObject>();

                if (currentObject != null)
                    currentObject.Focus();
            }
        }
    }
}
