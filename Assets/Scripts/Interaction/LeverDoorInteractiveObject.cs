﻿using UnityEngine;
using Util;

namespace Interaction.Detail
{
    public class LeverDoorInteractiveObject : InteractiveObject
    {
        [SerializeField]
        private TweenerSettings settings;
        [SerializeField]
        private LevelDoorItem[] items;
        

        private bool canInteract = true;
        private TweenerController tweenController = new TweenerController();

        public override bool CanInteract
        {
            get { return canInteract && base.CanInteract; }
            protected set { base.CanInteract = value; }
        }

        public override InteractiveObjectBehaviour Behaviour
        {
            get { return null; }
        }

        public override bool CanPickUp
        {
            get { return false; }
        }

        public override InteractiveObjectType TypeToUse
        {
            get { return InteractiveObjectType.NONE; }
        }

        public override void StartInteraction()
        {
            canInteract = false;
            for(int  i = 0; i< items.Length; i++)
                tweenController.Move(items[i].Content, items[i].OpenPoint.position, settings, null);
            Unfocus();
        }
    }
}
