﻿using UnityEngine;
using Zenject;
using GameInput;

namespace Player.Detail
{
    public class PlayerCameraController : MonoBehaviour
    {
        [SerializeField]
        private Transform myTransform;
        [SerializeField]
        private Transform characterTransform;
        [SerializeField]
        private PlayerCameraControllerSettings settings;

        [Inject]
        private IInputController inputController;

        private Vector2 mouseLook;
        private Vector2 smoothVelocity;
        
        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            Vector2 cameraAxis = inputController.CameraAxis;
            cameraAxis = Vector2.Scale(cameraAxis, Vector2.one * settings.Sensitivity * settings.Smoothing);
            smoothVelocity.x = Mathf.Lerp(smoothVelocity.x, cameraAxis.x, 1f / settings.Smoothing);
            smoothVelocity.y = Mathf.Lerp(smoothVelocity.y, cameraAxis.y, 1f / settings.Smoothing);
            mouseLook += smoothVelocity;
            mouseLook.y = Mathf.Clamp(mouseLook.y, settings.MinYAngle, settings.MaxYAngle);
            myTransform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
            characterTransform.localRotation = Quaternion.AngleAxis(mouseLook.x, characterTransform.up);
        }
    }
}
