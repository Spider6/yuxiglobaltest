﻿using UnityEngine;
using GameInput;
using Zenject;
using Battle;
using Util;

namespace Player.Detail
{
    public class PlayerWepon : MonoBehaviour
    {
        [SerializeField]
        private WeaponBase weapon;

        [Inject]
        private IInputController inputController;
        [Inject]
        private LayerController layerController;

        private void Update()
        {
            if (inputController.IsFireButonPressed)
                weapon.TryAttack(layerController.PlayerLayer);
        }
    }
}
