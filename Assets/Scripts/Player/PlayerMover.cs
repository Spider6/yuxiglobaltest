﻿using UnityEngine;
using GameInput;
using Zenject;

namespace Player.Detail
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody myRigidbody;
        [SerializeField]
        private Transform myTransform;
        [SerializeField]
        private PlayerMoverSettings settings;

        [Inject]
        private IInputController inputController;

        private Vector3 lastMotion;

        private void LateUpdate()
        {
            Vector2 moveAxis = inputController.MoveAxis;
            Vector3 moveVector =  (transform.forward *moveAxis.y + transform.right*moveAxis.x)*settings.Speed*Time.deltaTime;

            myRigidbody.AddForce(moveVector);
            moveVector.y = myRigidbody.velocity.y;
            myRigidbody.velocity = moveVector;
        }
    }
}
