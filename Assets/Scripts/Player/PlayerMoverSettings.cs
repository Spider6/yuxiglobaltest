﻿using UnityEngine;

namespace Player.Detail
{
    public class PlayerMoverSettings : ScriptableObject
    {
        [SerializeField]
        private float speed;

        public float Speed
        {
            get { return speed; }
        }
    }
}
