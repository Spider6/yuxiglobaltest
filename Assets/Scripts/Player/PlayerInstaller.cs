﻿using Zenject;
using UnityEngine;

namespace Player.Detail.Installer
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField]
        private PlayerController playerController;

        public override void InstallBindings()
        {
            Container.Bind<IPlayerController>().FromInstance(playerController);
            Container.Bind<IPlayerControllerForHeal>().FromInstance(playerController);
        }
    }
}
