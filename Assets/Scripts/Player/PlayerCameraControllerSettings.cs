﻿using UnityEngine;

namespace Player.Detail
{
    public class PlayerCameraControllerSettings : ScriptableObject
    {
        [SerializeField]
        private float sensitivity = 2;
        [SerializeField]
        private float smoothing = 5;
        [SerializeField]
        private float minYAngle;
        [SerializeField]
        private float maxYAngle;

        public float Sensitivity
        {
            get { return sensitivity; }
        }

        public float Smoothing
        {
            get { return smoothing; }
        }

        public float MinYAngle
        {
            get { return minYAngle; }
        }

        public float MaxYAngle
        {
            get { return maxYAngle; }
        }
    }
}
