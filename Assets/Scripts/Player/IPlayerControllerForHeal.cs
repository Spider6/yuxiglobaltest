﻿namespace Player
{
    public interface IPlayerControllerForHeal
    {
        void Heal(float healAmount);
        void SetDamage(float damage);
    }
}