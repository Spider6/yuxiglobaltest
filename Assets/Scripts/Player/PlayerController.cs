﻿using UnityEngine;
using Battle;
using System;
using Zenject;
using Interaction;
using GameInput;

namespace Player.Detail
{
    public class PlayerController : MonoBehaviour, IPlayerControllerForHeal, IPlayerController
    {
        public event Action DiedDelegate;
        public event Action ChangedHealthDelegate;
        
        [SerializeField]
        private HealthController healthController;
        [SerializeField]
        private PlayerMover mover;
        [SerializeField]
        private PlayerWepon weaponController;
        
        [Inject]
        private IInputController inputController;
        [Inject]
        private IInventory inventory;

        public float HealthPrcnt
        {
            get { return (healthController.CurrentHealth / healthController.MaxHealth) * 100; }
        }

        public Vector3 Position
        {
            get { return transform.position; }
        }

        public void Heal(float healAmount)
        {
            healthController.Heal(healAmount);
        }

        public void SetDamage(float damage)
        {
            healthController.SetDamage(damage);
        }

        private void OnDied()
        {
            mover.enabled = false;
            weaponController.enabled = false;

            if (DiedDelegate != null)
                DiedDelegate();
        }

        private void OnChangedHealth()
        {
            if (ChangedHealthDelegate != null)
                ChangedHealthDelegate();
        }

        private void Start()
        {
            healthController.Initialize();
            healthController.DiedDelegate += DiedDelegate;
            healthController.HitDelegate += ChangedHealthDelegate;
            healthController.HealedDelegate += ChangedHealthDelegate;
            OnChangedHealth();
        }

        private void Update()
        {
            if (inputController.HealButtonWasPressed && inventory.HasObjectOfType(InteractiveObjectType.HEALTH_BOTTLES))
                inventory.UseObjectOfType(InteractiveObjectType.HEALTH_BOTTLES);
        }
    }
}
