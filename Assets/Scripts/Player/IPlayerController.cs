﻿using System;
using UnityEngine;

namespace Player
{
    public interface IPlayerController
    {
        event Action ChangedHealthDelegate;
        event Action DiedDelegate;

        float HealthPrcnt { get; }
        Vector3 Position { get; }

    }
}