﻿using Zenject;
using Player;
using Interaction;
using Enemies;

namespace UIController.Detail
{
    public class HudController : WindowBase
    {
        [Inject]
        private IPlayerController playerController;
        [Inject]
        private IInventory inventory;
        [Inject]
        private IEnemiesController enemiesController;

        private IHealthSetter[] healthSetters;
        private IInventorySetter[] inventorySetters;
        private IScoreSetter[] scoreSetters;

        protected override void OnBack() { }

        [Inject]
        private void PostInject()
        {
            playerController.DiedDelegate += OnChangeHealth;
            playerController.ChangedHealthDelegate += OnChangeHealth;
            inventory.ChangedInventoryDelegate += OnChangedInventory;
            enemiesController.ScoreChangedDelegate += OnScoreChanged;
        }

        private void OnScoreChanged()
        {
            for (int i = 0; i < scoreSetters.Length; i++)
                scoreSetters[i].SetUp(enemiesController.CurrentScore.ToString());
        }

        private void OnChangedInventory(InteractiveObjectType type)
        {
            int amount = inventory.GetAmountOfType(type);
            for (int i = 0; i < inventorySetters.Length; i++)
                inventorySetters[i].SetUp(type, amount);
        }

        private void OnChangeHealth()
        {
            for (int i = 0; i < healthSetters.Length; i++)
                healthSetters[i].SetUp(playerController.HealthPrcnt.ToString() + "%");
        }

        private void Start()
        {
            healthSetters = gameObject.GetComponentsInChildren<IHealthSetter>(true);
            inventorySetters = gameObject.GetComponentsInChildren<IInventorySetter>(true);
            scoreSetters = gameObject.GetComponentsInChildren<IScoreSetter>(true);
            OnChangedInventory(InteractiveObjectType.HEALTH_BOTTLES);
            OnChangedInventory(InteractiveObjectType.KEYS);
            OnScoreChanged();
        }
    }
}
