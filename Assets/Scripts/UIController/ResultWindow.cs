﻿using Player;
using Zenject;
using UnityEngine;
using Enemies;
using Util;

namespace UIController.Detail
{
    public class ResultWindow : WindowBase
    {
        [SerializeField]
        private GameObject dieContent;
        [SerializeField]
        private GameObject winContent;

        [Inject]
        private IPlayerController playerController;
        [Inject]
        private IEnemiesController enemiesController;
        [Inject]
        private SceneController sceneController;
        
        public void PlayAgain()
        {
            sceneController.LoadGameScene();
        }

        public void GoToStartScene()
        {
            sceneController.LoadStartScene();
        }

        protected override void OnBack() {}
        
        [Inject]
        private void PostInject()
        {
            playerController.DiedDelegate += OnDied;
            enemiesController.KilledAllDelegate += OnWin;
        }

        private void OnWin()
        {
            Show(true);
        }

        private void OnDied()
        {
            Show(false);
        }

        private void Show(bool win)
        {
            IScoreSetter[] scoreSetters = gameObject.GetComponentsInChildren<IScoreSetter>(true);
            for (int i = 0; i < scoreSetters.Length; i++)
                scoreSetters[i].SetUp(enemiesController.CurrentScore.ToString());
            dieContent.SetActive(!win);
            winContent.SetActive(win);
            Cursor.lockState = CursorLockMode.None;
            Show();
        }
    }
}
