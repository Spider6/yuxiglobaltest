﻿using System.Collections.Generic;

namespace UIController.Detail
{
	public class WindowManager 
	{
        private Stack<Window> historyWindow = new Stack<Window>();
		private Window currentWindow;
        
		public void OnShow(Window newWindow)
		{
            if (currentWindow != null && currentWindow != newWindow) 
			{
                currentWindow.Hide ();
				historyWindow.Push (currentWindow);
			}
			currentWindow = newWindow ;
		}

		public void OnClose()
		{
           	currentWindow = historyWindow.Count > 0 ? historyWindow.Pop () : null;
			if (currentWindow != null) 
				currentWindow.Show ();
		}
	}
}