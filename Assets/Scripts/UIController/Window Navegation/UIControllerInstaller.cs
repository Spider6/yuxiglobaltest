using Zenject;

namespace UIController.Detail.ZenjectInstaller
{
    public class UIControllerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<WindowManager>().AsSingle();
        }
    }
}