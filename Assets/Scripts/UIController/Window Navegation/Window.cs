using UnityEngine;
using System;

namespace UIController
{
	public abstract class Window : MonoBehaviour
	{
		public event Action ShownDelegate;
		public event Action ClosedDelegate;

        public virtual bool CanBack
        {
            get { return true; }
        }

        public abstract void Hide();
        public abstract void Show();
        public abstract void Close();
        
        public void Back()
        {
            if (CanBack)
                OnBack();
        }

        protected abstract void OnBack();

        protected void OnShown()
        {
            if (ShownDelegate != null)
                ShownDelegate();
        }

        protected void OnClosed()
        {
            if (ClosedDelegate != null)
                ClosedDelegate(); 
        }
	}
}