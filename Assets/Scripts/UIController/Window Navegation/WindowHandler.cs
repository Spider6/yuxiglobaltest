using UnityEngine;
using Zenject;

namespace UIController.Detail
{
	public class WindowHandler : MonoBehaviour
	{
		[SerializeField]
		private Window window;

        [Inject]
        private WindowManager windowManager;

        private void Awake()
		{
			window.ShownDelegate += OnShow;
			window.ClosedDelegate += OnClose;
		}

		private void OnShow()
		{
			windowManager.OnShow (window);
		}

		private void OnClose()
		{
			windowManager.OnClose ();
		}
	}
}