﻿namespace UIController
{
    public interface ITextSetter
    {
        void SetUp(string newText);
    }
}
