﻿namespace UIController
{
    public interface IHealthSetter : ITextSetter
    {
    }

    public interface IScoreSetter : ITextSetter
    {
    }
}
