using System;

namespace UIController
{
    public interface IWindowFader
    {
        void FadeIn(Action callback);
        void FadeOut(Action callback);
    }
}