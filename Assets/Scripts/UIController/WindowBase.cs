﻿using UnityEngine;

namespace UIController
{
    public abstract class WindowBase : Window
    {
        [SerializeField]
        protected RectTransform content;
        [SerializeField]
        private bool showAtStart;
        
        private IWindowFader fader;

        public override void Close()
        {
            Hide();
            OnClosed();
        }

        public override void Hide()
        {
            fader.FadeOut(OnHidden);
        }

        public override void Show()
        {
            content.gameObject.SetActive(true);
            fader.FadeIn(null);
            OnShown();
        }

        protected virtual void ForceHide()
        {
            content.gameObject.SetActive(false);
        }

        private void OnHidden()
        {
            content.gameObject.SetActive(false);
        }
        
        private void Start()
        {
            fader = GetComponentInChildren<IWindowFader>();
            ForceHide();
            if (showAtStart)
                Show();
        }
    }

}
