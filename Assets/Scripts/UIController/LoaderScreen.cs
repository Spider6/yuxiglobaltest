﻿using UnityEngine;
using Util;

namespace UIController.Detail
{ 
    public class LoaderScreen : MonoBehaviour, ILoaderScreen
    {
        [SerializeField]
        private GameObject content;

        private int calls;
        private IWindowFader fader;

        public void Show()
        {
            calls++;
            if (calls == 1)
            {
                content.SetActive(true);
                fader.FadeIn(null);
            }
        }

        public void Close()
        {
            if (calls > 0)
            {
                if (--calls == 0)
                    fader.FadeOut(OnClosed);
            }
        }

        private void OnClosed()
        {
            content.gameObject.SetActive(false);
        }

        private void Awake()
        {
            fader = gameObject.GetComponentInChildren<IWindowFader>();
            calls = 1;
            Close();
        }
    }
}
