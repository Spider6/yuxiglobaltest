﻿using Zenject;
using Util;
using UnityEngine;

namespace UIController.Detail
{
    public class StartWindow : WindowBase
    {
        [Inject]
        private SceneController sceneController;

        protected override void OnBack() { }

        public void StartTest()
        {
            sceneController.LoadGameScene();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
