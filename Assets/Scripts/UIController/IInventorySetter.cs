﻿using Interaction;

namespace UIController
{
    public interface IInventorySetter 
    {
        void SetUp(InteractiveObjectType type, int amount);
    }
}
