﻿using System;
using UnityEngine;

namespace Enemies.Detail
{
    public abstract class EnemyVisibilityController : MonoBehaviour
    {
        public event Action EnteredDelegate;
        public event Action ExitDelegate;

        protected void OnEntered()
        {
            if (EnteredDelegate != null)
                EnteredDelegate();
        }

        protected void OnExit()
        {
            if (ExitDelegate != null)
                ExitDelegate();
        }
    }
}
