﻿using UnityEngine;

namespace Enemies.Detail
{
    public class EnemyDestroyer : MonoBehaviour
    {
        [SerializeField]
        private GameObject root;

        public void Destroy()
        {
            Destroy(root);
        }
    }
}
