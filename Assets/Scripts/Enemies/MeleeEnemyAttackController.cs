﻿using UnityEngine;
using Battle;
using Zenject;
using Util;
using Player;

namespace Enemies.Detail
{
    public class MeleeEnemyAttackController : EnemyAttackController
    {
        [SerializeField]
        private EnemyAnimationHandler animationHandler;

        [Inject]
        private LayerController layerController;
        [Inject]
        private IPlayerControllerForHeal playerController;

        public override void StartAttack()
        {
            animationHandler.PlayAttack();
        }

        public void SetDamage()
        {
            playerController.SetDamage(10);
            OnEndAttack();
        }
    }
}
