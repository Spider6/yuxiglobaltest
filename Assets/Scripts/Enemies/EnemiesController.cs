﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies.Detail
{
    public class EnemiesController : MonoBehaviour, IEnemiesController
    {
        public event Action ScoreChangedDelegate;
        public event Action KilledAllDelegate;


        [SerializeField]
        private List<EnemyController> enemies;

        public int CurrentScore
        {
            get;
            private set;
        }

        private void Awake()
        {
            enemies.ForEach(e => e.DiedDelegate += OnEnemyDied);
            CurrentScore = 0;
        }

        private void OnEnemyDied(EnemyController enemy)
        {
            enemies.Remove(enemy);
            CurrentScore += 100;
            if (ScoreChangedDelegate != null)
                ScoreChangedDelegate();
            if (enemies.Count == 0 && KilledAllDelegate != null)
                KilledAllDelegate();
    }
    }
}
