﻿using UnityEngine;

namespace Enemies.Detail
{
    public class EnemyAnimationHandler : MonoBehaviour
    {
        [SerializeField]
        private Animator myAnimator;
        [SerializeField]
        private EnemyAnimationHandlerSettings settings;

        public void PlayRun()
        {
            myAnimator.SetTrigger(settings.RunAnimationName);
        }

        public void PlayAttack()
        {
            myAnimator.SetTrigger(settings.AttackAnimationName);
        }

        public void PlayIdle()
        {
            myAnimator.SetTrigger(settings.IdleAnimationName);
        }

        public void PlayDead()
        {
            myAnimator.SetTrigger(settings.DieAnimationName);
        }
    }
}
