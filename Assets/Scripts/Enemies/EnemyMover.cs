﻿using System;
using UnityEngine;

namespace Enemies.Detail
{
    public abstract class EnemyMover : MonoBehaviour
    {
        public event Action ReachedDelegate;

        public abstract void StartMove();
        public abstract void StartSeek();

        protected void OnReachedTarget()
        {
            if (ReachedDelegate != null)
                ReachedDelegate();
        }
    }
}
