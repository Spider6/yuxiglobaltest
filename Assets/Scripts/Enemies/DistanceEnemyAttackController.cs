﻿using UnityEngine;
using Battle;
using Zenject;
using Util;
using Player;

namespace Enemies.Detail
{
    public class DistanceEnemyAttackController : EnemyAttackController
    {
        [SerializeField]
        private EnemyAnimationHandler animationHandler;
        [SerializeField]
        private Projectile projectilePrefab;
        [SerializeField]
        private Transform shootPoint;

        [Inject]
        private LayerController layerController;
        [Inject]
        private IPlayerController playerController;

        public override void StartAttack()
        {
            animationHandler.PlayAttack();
        }

        public void SetDamage()
        {
            Projectile projectile = Instantiate(projectilePrefab);
            projectile.transform.position = shootPoint.position;
            Vector3 direction = playerController.Position - shootPoint.position;
            projectile.transform.rotation = Quaternion.LookRotation(direction);
            projectile.OnShoot(layerController.EnemyLayer);
            OnEndAttack();
        }
    }
}
