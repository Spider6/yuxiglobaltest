﻿using Zenject;
using UnityEngine;

namespace Enemies.Detail.Installer
{
    public class EnemiesInstaller :MonoInstaller
    {
        [SerializeField]
        private EnemiesController enemiesController;

        public override void InstallBindings()
        {
            Container.Bind<IEnemiesController>().FromInstance(enemiesController);
        }
    }
}
