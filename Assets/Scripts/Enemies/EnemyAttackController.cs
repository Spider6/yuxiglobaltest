﻿using System;
using UnityEngine;

namespace Enemies.Detail
{
    public abstract class EnemyAttackController : MonoBehaviour
    {
        public event Action EndedAttackDelegate;

        public abstract void StartAttack();

        protected void OnEndAttack()
        {
            if(EndedAttackDelegate != null)
                EndedAttackDelegate();
        }
    }
}
