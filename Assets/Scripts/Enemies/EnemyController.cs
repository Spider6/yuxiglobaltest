﻿using Battle;
using System;
using UnityEngine;

namespace Enemies.Detail
{
    public class EnemyController : MonoBehaviour
    {
        public event Action<EnemyController> DiedDelegate;

        [SerializeField]
        private EnemyAnimationHandler animationHandler;
        [SerializeField]
        private HealthController healthController;
        [SerializeField]
        private EnemyMover mover;
        [SerializeField]
        private EnemyVisibilityController visibilityController;
        [SerializeField]
        private EnemyAttackController attackController;

        private bool isInArea;

        private void Awake()
        {
            healthController.Initialize();
            healthController.DiedDelegate += OnDied;
            mover.ReachedDelegate += OnReachedTarget;
            visibilityController.EnteredDelegate += OnEnteredArea;
            visibilityController.ExitDelegate += OnExitArea;
            attackController.EndedAttackDelegate += OnEndedAttack;
        }

        private void OnEndedAttack()
        {
            if(isInArea)
                mover.StartSeek();
            else
                mover.StartMove();
        }

        private void OnExitArea()
        {
            isInArea = false;
            mover.StartMove();
        }

        private void OnEnteredArea()
        {
            isInArea = true;
            mover.StartSeek();
        }

        private void OnReachedTarget()
        {
            attackController.StartAttack();
        }

        private void OnDied()
        {
            animationHandler.PlayDead();

            if (DiedDelegate != null)
                DiedDelegate(this);
        }
    }
}
