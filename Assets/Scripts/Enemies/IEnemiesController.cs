﻿using System;

namespace Enemies
{
    public interface IEnemiesController
    {
        event Action KilledAllDelegate;
        event Action ScoreChangedDelegate;
        int CurrentScore { get; }
    }
}