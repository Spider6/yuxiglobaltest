﻿using UnityEngine;
using Zenject;
using Util;

namespace Enemies.Detail
{
    public class EnemyVisibilityTriggerController : EnemyVisibilityController
    {
        [Inject]
        private LayerController layerController;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == layerController.PlayerLayer)
                OnEntered();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == layerController.PlayerLayer)
                OnExit();
        }
    }
}
