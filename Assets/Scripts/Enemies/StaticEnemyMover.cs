﻿using UnityEngine;

namespace Enemies.Detail
{
    public class StaticEnemyMover : EnemyMover
    {
        [SerializeField]
        private EnemyAnimationHandler animationHandler;

        public override void StartMove()
        {
            animationHandler.PlayIdle();
        }

        public override void StartSeek()
        {
            OnReachedTarget();
        }
    }
}
