﻿using UnityEngine;

namespace Enemies.Detail
{
    public class EnemyAnimationHandlerSettings : ScriptableObject
    {
        [SerializeField]
        private string runAnimationName;
        [SerializeField]
        private string dieAnimationName;
        [SerializeField]
        private string idleAnimationName;
        [SerializeField]
        private string attackAnimationName;

        public string RunAnimationName
        {
            get { return runAnimationName; }
        }

        public string DieAnimationName
        {
            get { return dieAnimationName; }
        }

        public string IdleAnimationName
        {
            get { return idleAnimationName; }
        }

        public string AttackAnimationName
        {
            get { return attackAnimationName; }
        }
    }
}
