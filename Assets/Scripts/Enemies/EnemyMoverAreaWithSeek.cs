﻿using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Player;

namespace Enemies.Detail
{
    public class EnemyMoverAreaWithSeek : EnemyMover
    {
        [SerializeField]
        private NavMeshAgent agent;
        [SerializeField]
        private Transform myTransform;
        [SerializeField]
        private Transform[] pointsArea;
        [SerializeField]
        private EnemyAnimationHandler animationHandler;

        [Inject]
        private IPlayerController playerController; 

        private int currentPointIndex;
        private bool isSeeking;
        private Transform currentTarget;

        private bool isMoving;

        public Vector3 CurrentPosition
        {
            get { return isSeeking ? playerController.Position : currentTarget.position; }
        }
        
        public override void StartMove()
        {
            animationHandler.PlayRun();
            isMoving = true;
            isSeeking = false;
            UpdateTarget();
        }

        public override void StartSeek()
        {
            animationHandler.PlayRun();
            isSeeking = true;
            isMoving = true;
        }

        private void Update()
        {
            if (isMoving)
            {
                if (Vector3.Distance(CurrentPosition, myTransform.position) <= agent.stoppingDistance)
                {
                    if (isSeeking)
                    {
                        isMoving = false;
                        OnReachedTarget();
                    }
                    else
                        UpdateTarget();
                }

                if (isSeeking && isMoving)
                    agent.SetDestination(CurrentPosition);
            }
            else
                agent.ResetPath();

        }
        
        private void UpdateTarget()
        {
            currentPointIndex++;
            if (currentPointIndex >= pointsArea.Length)
                currentPointIndex = 0;
            currentTarget = pointsArea[currentPointIndex];
            agent.SetDestination(currentTarget.position);
        }


        private void Start()
        {
            StartMove();
        }
    }
}
