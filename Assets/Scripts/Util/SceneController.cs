using UnityEngine.SceneManagement;
using UnityEngine;
using Util.Detail;
using System.Collections;
using Zenject;

namespace Util
{
    public class SceneController : MonoBehaviour
    {
        [SerializeField]
        private SceneControllerSettings settings;

        [Inject]
        private ILoaderScreen loaderScreen;

        public void LoadStartScene()
        {
            StartCoroutine(LoadScene(settings.StartSceneName));
        }

        public void LoadGameScene()
        {
            StartCoroutine(LoadScene(settings.GameSceneName));
        }

        private IEnumerator LoadScene(string sceneName)
        {
            loaderScreen.Show();
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            loaderScreen.Close();
        }
    }
}