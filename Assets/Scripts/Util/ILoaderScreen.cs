namespace Util
{
    public interface ILoaderScreen
    {
        void Show();
        void Close();
    }
}