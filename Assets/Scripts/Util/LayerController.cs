﻿using UnityEngine;
using Util.Detail;

namespace Util
{
    [System.Serializable]
    public class LayerController
    {
        [SerializeField]
        private LayerControllerSettings settings;

        public int PlayerLayer
        {
            get { return LayerMask.NameToLayer(settings.PlayerLayerName); }
        }

        public int InteractiveLayer
        {
            get { return LayerMask.NameToLayer(settings.InteractiveLayerName); }
        }

        public int EnemyLayer
        {
            get { return LayerMask.NameToLayer(settings.EnemyLayerName); }
        }
    }
}
