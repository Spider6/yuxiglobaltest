using DG.Tweening;
using UnityEngine;

namespace Util
{
    public class RotationTweenerSettings : TweenerSettings
    {
        [SerializeField]
        private RotateMode mode;
        [SerializeField]
        private Vector3 euler;

        public RotateMode Mode
        {
            get { return mode; }
        }

        public Vector3 Euler
        {
            get { return euler; }
        }
    }

}