using UnityEngine;

namespace Util.Detail
{
    public class SceneControllerSettings : ScriptableObject
    {
        [SerializeField]
        private string startSceneName;
        [SerializeField]
        private string gameSceneName;
        
        public string StartSceneName
        {
            get { return startSceneName; }
        }

        public string GameSceneName
        {
            get { return gameSceneName; }
        }
    }
}