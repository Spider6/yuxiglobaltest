using UnityEngine;
using UIController.Detail;
using Zenject;

namespace Util.Detail.Installer
{
    public class UtilInstaller : MonoInstaller
    {
        [SerializeField]
        private LoaderScreen loaderScreen;
        [SerializeField]
        private SceneController sceneController;
        [SerializeField]
        private LayerController layerController;

        public override void InstallBindings()
        {
            Container.Bind<ILoaderScreen>().FromInstance(loaderScreen);
            Container.Bind<SceneController>().FromInstance(sceneController);
            Container.Bind<LayerController>().FromInstance(layerController);
        }
    }

}