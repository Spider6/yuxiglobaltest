using DG.Tweening;
using UnityEngine;

namespace Util
{
    public class TweenerSettings : ScriptableObject
    {
        [SerializeField]
        private float delay;
        [SerializeField]
        private float duration;
        [SerializeField]
        private Ease easeType;

        public float Delay
        {
            get { return delay; }
        }

        public float Duration
        {
            get { return duration; }
        }

        public Ease EaseType
        {
            get { return easeType; }
        }
    }

}