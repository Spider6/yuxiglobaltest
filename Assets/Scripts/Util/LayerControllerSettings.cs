﻿using UnityEngine;

namespace Util.Detail
{
    public class LayerControllerSettings : ScriptableObject
    {
        [SerializeField]
        private string playerLayerName;
        [SerializeField]
        private string enemyLayerName;
        [SerializeField]
        private string interactiveLayerName;

        public string PlayerLayerName
        {
            get { return playerLayerName; }

        }

        public string EnemyLayerName
        {
            get { return enemyLayerName; }
        }

        public string InteractiveLayerName
        {
            get { return interactiveLayerName; }
        }
    }
}
