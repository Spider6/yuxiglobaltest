﻿using DG.Tweening;
using System;
using UnityEngine;

namespace Util
{
    public class TweenerController
    {
        public void Stop(Transform content)
        {
            content.DOKill();
        }

        public void Move(Transform content, Vector3 position, TweenerSettings settings, Action callback = null)
        {
            Tweener tweener = content.DOMove(position, settings.Duration);
            tweener.SetEase(settings.EaseType).SetDelay(settings.Delay);
            if (callback != null)
                tweener.OnComplete(() => { callback(); });
        }

        public void Fade(CanvasGroup content, float alpha, TweenerSettings settings, Action callback = null)
        {
            Tweener tweener = content.DOFade(alpha, settings.Duration);
            tweener.SetEase(settings.EaseType).SetDelay(settings.Delay);
            if (callback != null)
                tweener.OnComplete(() => { callback(); });
        }
        
        public void MoveRect(RectTransform content, Vector2 position, TweenerSettings settings, Action callback = null)
        {
            Tweener tweener = content.DOAnchorPos(position, settings.Duration);
            tweener.SetEase(settings.EaseType).SetDelay(settings.Delay);
            if (callback != null)
                tweener.OnComplete(() => { callback(); });
        }

        public void Scale(Transform content, Vector3 scale, TweenerSettings settings, Action callback = null)
        {
            Tweener tweener = content.DOScale(scale, settings.Duration);
            tweener.SetEase(settings.EaseType).SetDelay(settings.Delay);
            if (callback != null)
                tweener.OnComplete(() => { callback(); });
        }

        public void Rotate(Transform content, RotationTweenerSettings settings, Action callback = null)
        {
            Tweener tweener = content.DORotate(settings.Euler, settings.Duration, settings.Mode);
            tweener.SetEase(settings.EaseType).SetDelay(settings.Delay);
            if (callback != null)
                tweener.OnComplete(() => { callback(); });
        }
    }
}
